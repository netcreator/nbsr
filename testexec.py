"""Module for testing Non-Blocking Stream Reader

This module has to be outside the nbsr package folder so that
relative path imports can be used
"""


from test import local


#if this module is run as a script, run the test cases
if __name__ == "__main__":
    local.Runner().start()
