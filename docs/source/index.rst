.. Non-Blocking Stream Reader documentation master file

Non-Blocking Stream Reader's documentation
======================================================

.. meta::
   :description: Non-Blocking Stream Reader documentation
   :keywords: Python2.7, Python3, Windows, I/O blocking

.. toctree:: modules
   :maxdepth: 2

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
