nbsr package
============

.. automodule:: nbsr
    :members:
    :undoc-members:
    :show-inheritance:

.. Submodules
.. ----------

*The* module
------------

.. automodule:: nbsr.nonblocking_reader
    :members:
    :undoc-members:

Exceptions
----------

.. automodule:: nbsr.exceptions
    :members:
    :undoc-members:

Constants
---------

.. automodule:: nbsr.returnbys
    :members:
    :undoc-members:
