"""Testing nbsr

This module is to implement the local process, i.e. the user of nbsr.NonBlockingStreamReader.
It first reads up the configuration that defines the dialogue. It then launches the remote
process and connects to it. Normally the remote process is an OS of a network element, but
we simulate it in this test with another process on this same PC. The remote element is
normally connected with using ``subprocess`` and establishing a pipe to it with SSH or
Telnet. This simulation does not use SSH or Telnet, as the remote process is just another
process on this same PC. Otherwise the communication is the same.

After the remote process is launched and pipe is connected, the remote process will simulate
a remote network element, i.e. provide prompt and answer the commands.

First the remote process will wait simulating a slow startup of the communication.
Then it will challange the local process with e.g. authentication. It could challange the local
process with pausing long startup, rejecting access, or other error situations. This phase
is called the 'initial interaction'.

After the initial interaction, the remote process provides the prompt and answers commands.
This phase is called 'command interaction'.

During command interaction the output of the remote process can be broken up by screen-
length section and a 'more' promp is displayed. Local process has to answer this, usually
sending a single space character. See the UNIX/LINUX ``more`` command.

The remote process can interpret a 'exit' command, terminate then, and simulating EOF on
the pipe.
"""


import os
import sys
import time
import subprocess
import ruamel.yaml

#import the nbsr module (whatever the import finder can see first). Python 2 has no adequate relative path import.
import nbsr


class RemoteError(Exception):
    pass


class LocalError(Exception):
    pass


class Runner(object):
    def __init__(self):
        #lines or partial lines will be collected here from the pipe stream
        self.buffer = []
        
        #read up config
        dir_path = os.path.dirname(sys.argv[0])
        conffilename = os.path.join(dir_path, 'test', 'conf.yml')
        with open(conffilename,  'r') as conffile:
            self.conf = ruamel.yaml.safe_load(conffile)['local']
        
        #set the flag indicating whether text stream is used
        self.textstream = self.conf['textstreamneeded']
        
        #convert all interaction case to bytes if not text stream
        if not self.textstream:
            for element in self.conf['init_interaction']:
                element['case'] = element['case'].encode('ASCII')
                if 'response' in element and element['response'] is not None:
                    element['response'] = element['response'].encode('ASCII')
            for element in self.conf['command_defs']:
                element['case'] = element['case'].encode('ASCII')
                if 'response' in element and element['response'] is not None:
                    element['response'] = element['response'].encode('ASCII')
                
        #extract the list of patterns (cases) for initial interaction
        self.init_patterns = tuple(p['case'] for p in self.conf['init_interaction'])
        
        #generator to yield the command sequence
        if self.textstream:
            self.commandsgen = (c for c in self.conf['command_sequence'])
        else:
            self.commandsgen = (c.encode('ASCII') for c in self.conf['command_sequence'])
        
        #extract the list of patterns (cases) for command interaction
        self.command_patterns = tuple(p['case'] for p in self.conf['command_defs'])
        
        #set the newline character according to the stream type
        self.newlinechar = '\n' if self.textstream else b'\n'
        self.eof_string = '<EOF>' if self.textstream else b'<EOF>'
        self.eval_prefix = 'eval-->' if self.textstream else b'eval-->'

    
    def start(self):
        """Run test cases"""
        #open the pipe to the remote NE
        self.pipe = subprocess.Popen(
                                                    ['python', 'test/remote.py'], 
                                                    stdin=subprocess.PIPE, 
                                                    stdout=subprocess.PIPE, 
                                                    stderr=subprocess.STDOUT, 
                                                    shell=False, 
                                                    text=self.textstream, 
                                                )
        #If shell is True, a cmd shell is created where the remote process (feeder) runs.
        #For some reason, when pipe is terminated, its output does not get closed and keeps
        #  sending space characters. Therefore the nbsr working thread won't terminate
        #  and this test run python script hangs even after sys.exit.
        #So, better to use shell=False
        
        try:
            #create the nbsr stream and start reading the remote output
            with nbsr.NonBlockingStreamReader(
                                                                stream=self.pipe.stdout,
                                                                timeout=self.conf['readtimeout']
                                                            ) as self.nb_stdout:
                #remember the start time as a base
                self.starttime = time.time()
                
                #wait until the remote process starts to send characters
                self._startup()
                
                #do initial interactions
                self._init_interaction()
                
                #do command interactions
                self._command_interaction()
                
                errortxt = None
                
        except LocalError as e:
            errortxt = str(e)
        
        finally:
            #terminate the pipe and remote process
            self.pipe.terminate()
            
            #print the communication
            print(''.join(self.buffer))
        
        #exit the program
        sys.exit(errortxt or 0)
    
    
    def _startup(self):
        """Start up the communication with the remote"""
        # wait for the remote to send data
        if not self.nb_stdout.is_sending(self.conf['waitforstartup']):
            self._printstatus()
            raise LocalError('Remote not sending data')
        self._put_to_buffer(inward=False, msg='started up\n')
        
        
    def _init_interaction(self):
        """Handle initial interaction"""
        while True:
            #read the remote output, store it and display progress
            lines = self._read(self.init_patterns)
            #only put anything ito the buffer if readlines is not an empty list
            if lines:
                for line in lines:
                    self._put_to_buffer(inward=True, msg=line)
            self._printstatus()
            
            #if pattern matched, respond
            if self.nb_stdout.return_reason() == nbsr.returnby_PATTERN:
                idx = self.nb_stdout.found_pattern_info()[0]
                response = self.conf['init_interaction'][idx]['response']
                #if there is no need to respond, exit initial interaction, otherwise respond
                if not response:
                    return
                else:
                    self._write(response + self.newlinechar)
                    continue
            
            #if there was a timeout (no any pattern matched, exit
            if self.nb_stdout.return_reason() == nbsr.returnby_TIMEOUT:
                raise LocalError('timeout')
            
            #if there was a timeout (no any pattern matched, exit
            if self.nb_stdout.return_reason() == nbsr.returnby_EOF:
                raise LocalError('EOF during initial interaction')
            
            else:
                raise LocalError('WTF?!')
        
        
    def _command_interaction(self):
        """Handle command interaction
        
        Returns:
            str: the error message, if any. Otherwise None
        """
        #get the first command (terminated by a newline)
        sendthis = self._getnextcommand()
        #loop on commands
        while True:
            #send the next command to the remote process
            self._write(sendthis)
            
            #read the remote output, store it and display progress
            lines = self._read(self.command_patterns)
            #only put anything ito the buffer if readlines is not an empty list
            if lines:
                for line in lines:
                    self._put_to_buffer(inward=True, msg=line)
            self._printstatus()
            
            return_reason = self.nb_stdout.return_reason()
            
            if return_reason == nbsr.returnby_PATTERN:
                idx = self.nb_stdout.found_pattern_info()[0]
                response = self.conf['command_defs'][idx].get('response')
                #if no response, finish the command interaction
                if not response:
                    return
                if self.eval_prefix in response:
                    response = eval(response[len(self.eval_prefix):])
                #if no response, finish the command interaction
                if not response:
                    return
                #if response is a text (or evaluated to a text), just send it
                if isinstance(response, (str, bytes)):
                    sendthis = response
                    continue
                #response is an exception?
                if isinstance(response, Exception):
                    raise response
                raise LocalError('Unknown response type: %s'%repr(response))
            
            elif return_reason == nbsr.returnby_TIMEOUT:
                raise LocalError('timeout')
            
            elif return_reason == nbsr.returnby_EOF:
                if lines and lines[-1] == self.eof_string:
                    return
                raise LocalError('End of comms')
            
            else:
                raise LocalError('WTF return reason: %i'%return_reason)
    
    
    def _read(self, patternlist):
        #read lines, a line or part of a line from the stdout of the remote process
        return self.nb_stdout.readlines(expected_patterns=patternlist)
    
    
    def _write(self, output):
        #write to the stdin of the remote process
        self.pipe.stdin.write(output)
        self.pipe.stdin.flush()
        
        #put the debugging message into the buffer
        self._put_to_buffer(inward=False, msg=output)
    
    
    def _getnextcommand(self):
        try:
            return next(self.commandsgen) + self.newlinechar
        except StopIteration:
            return None
    
    
    def _printstatus(self):
        """Print a formatted status after a read"""
        reason = self.nb_stdout.return_reason()
        
        if reason == nbsr.returnby_PATTERN:
            idx = self.nb_stdout.found_pattern_info()[0]
            details = 'returned by matching pattern index %d' % idx
        
        elif reason == nbsr.returnby_SIZE:
            details = 'returned because enough data read, which is normal'
        
        elif reason == nbsr.returnby_NEWLINE:
            details = 'returned because of new line, which is normal'
            
        elif reason == nbsr.returnby_TIMEOUT:
            details = 'returned because of timeout'
        
        elif reason == nbsr.returnby_EOF:
            details = 'returned because of EOF'
        
        else:
            details = 'what reason:%d' % reason
        
        #put the debugging message into the buffer
        self._put_to_buffer(inward=False, msg=details)


    def _put_to_buffer(self, inward, msg):
        #get the seconds that have elapsed since the local process started
        elapsed = time.time()-self.starttime
        
        #state the direction with a string
        dirstr = '<<' if inward else '>>'
        
        #print the elapsed time and the reason of return of the reading function
        self.buffer.append('%s%.9f: %s'% (dirstr, elapsed, msg))
