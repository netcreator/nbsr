"""The remote process simulation"""


import sys
import os
import time
import re
import ruamel.yaml


class Simulator(object):
    """Simulate a remote process
    
    The local process creates this remote process simulation, connects to its
    stdin, stdout (and optionally the stderr) via the pipes, then drives the
    communication.
    
    This remote process simulates slow startup, initial interaction (e.g.
    prompting for a password), then command interaction.
    
    The remote process gets commands on its stdin from the local process and
    responds to the commands on its stdout.
    
    The local process can terminate the command interaction either by receiving
    the kill command (which does not have an answer), or terminating the pipes.
    Either way, this remote process will terminate.
    
    The simulated communication can be configured in the YAML file called
    'conf.yml' in the same directory where this module file is.
    
    Attributes:
        conf (dict): the configuration, containg the elements listed below.
            
            - starttime (float): time in seconds that the remote process start up
            - prompt (str): prompt string
            - echo (bool): whether the received command is to be echoed
            - init_interaction (dict): named set of output/expect/next strings. This
                interaction is played before the prompt, simulating e.g. authentication.
                It starts with 'startwith' and contiues with next items intil next is
                empty or '***exit***'. Empty next item means the end of the initial
                interaction and the beginning of command interaction. Exit can be used
                in error cases, when this remote simulation terminates immediately.
            - command_interaction (list): the dialogue config consisting of patterns
                of input and their corresponding answers. If the input does not match
                to any of the patterns, the 'default_answer' is used.
            - eof_indicator (str): the string the remote process sends right
                before EOF
            - lines_before_more (int): if given and grater than 0, 'more' interaction
                takes place after that many lines
    """
    def __init__(self):
        #read up remote config
        self.dir_path = os.path.dirname(sys.argv[0])
        conffilename = os.path.join(self.dir_path, 'conf.yml')
        with open(conffilename,  'r') as conffile:
            self.conf = ruamel.yaml.safe_load(conffile)['remote']
    
    
    def run(self):
        try:
            #simulate the slow remote process setup
            time.sleep(self.conf['starttime'])
            
            #simulate the initial interaction
            self._init_interaction()
            
            #simulate the command interaction
            self._command_interaction()
        
        except Exception as e:
            self._flushedprint('Exception: %s\n'%repr(e))
    
    
    def _init_interaction(self):
        #load up the initial interaction config
        init_interaction = self.conf['init_interaction']
        #set the name of the 1st initial interaction
        init_name = init_interaction['startwith']
        
        #loop on the initial interaction cases chain
        while True:
            #if no more initial interaction, break here to continue with command interaction
            if not init_name:
                break
            init_case = init_interaction[init_name]
            #output the challange to the local process
            self._flushedprint(init_case['output'])
            init_name = init_case['next']
            if init_name == '***exit***':
                sys.exit(0)
            #read the answer from the local process (e.g. a password)
            inp = sys.stdin.readline().rstrip()
            #echo if needed
            self._do_echo(inp)
            #compare init_case['expect'] to inp
            #TODO: implement inp check
    
    
    def _command_interaction(self):
        #read commands in loop
        while True:
            #print the prompt
            self._flushedprint(self.conf['prompt'])
            #read input line
            inp = sys.stdin.readline()
            
            #echo if needed
            self._do_echo(inp)
            
            #strip off white spaces (e.g. the new line) at the end
            inp=inp.rstrip()
            #loop on all dialogue configs to provide the correct answer output
            for dialogue in self.conf['command_interaction']:
                #check whether the input matches a pattern
                matchObj = re.match(dialogue['pattern'], inp)
                if matchObj is not None:
                    answer = dialogue.get('answer')
                    if not answer:
                        #print the EOF text and exit
                        self._flushedprint(self.conf['eof_indicator'])
                        return
                    
                    if 'fromfile-->' in answer:
                        answerfilename = os.path.join(self.dir_path, answer[11:])
                        self._sendfile(answerfilename)
                        self._flushedprint('\n')
                    else:
                        #answer the pattern (substitute matched named groups)
                        self._flushedprint(
                            answer.format(**matchObj.groupdict())
                        )
                    break
            else:
                self._flushedprint(self.conf['default_answer']% inp)
    
    
    def _sendfile(self, answerfilename):
        with open(answerfilename, 'r') as f:
            linesleft = self.conf['lines_before_more']
            while True:
                line = f.readline()
                if not line:
                    break
                
                #print the line from the file
                self._flushedprint(line)
                
                linesleft -= 1
                
                #do 'more' interaction
                if not linesleft:
                    #print 'more' prompt
                    self._flushedprint(self.conf['more_prompt'])
                    
                    #read input character (answer to a 'more' prompt)
                    #Note: The IO by default is line-buffered if keyboard input, meaning waiting for newline char!
                    inp = sys.stdin.read(1)
                    self._do_echo(inp)
                    
                    #recover the 'more' prompt with a terminal excape sequence
                    self._flushedprint(self.conf['more_recovery'])
                    
                    #restore the number of lines left
                    linesleft = self.conf['lines_before_more']
    
    
    def _do_echo(self, txt):
        """Echo the received text if needed"""
        if self.conf['echo']:
            #echo the received input and terminate with new line
            self._flushedprint('Echoed:"%s"\n'%txt)
            
        
    def _flushedprint(self, msg):
        """Print a string and flush it"""
        #new line characters arrive in conf with excaped back-slash, so replace them with normal new line char
        print(msg.replace(r'\n', '\n'), end='', flush=True)


#if this module is run as a script, simulate the remote process
if __name__ == "__main__":
    Simulator().run()
