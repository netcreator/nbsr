"""Testing nbsr

This module is to implement the local process, i.e. the user of nbsr.NonBlockingStreamReader.
It first reads up the configuration that defines the dialogue. It then launches the remote
process and connects to it. Normally the remote process is a network element OS, but we
simulate it in this test with another process on this same PC. The remote element is
normally connected with using ``subprocess`` and establishing a pipe to it with SSH or
Telnet. This simulation uses ``multiprocess``, so that we can set up and launch it from this
same ``Runner`` class. Anyway, a parallel process is used as a remote and pipe is
established for the communication.

Since ``multiprocess`` pipe is too high level, we rather use ``IO.pipe`` so that we can
simulate the non-buffered, non-predefined query/reply dialogues.

After the remote process is launched and pipe is connected, the remote process will simulate
a remote network element, i.e. provide prompt and answer commands.

First the local process will wait simulating a slow startup of the communication.
Then it will challange the local process with e.g. authentication. It could challange the local
process with pausing long startup, rejecting access, or other error situations. This phase
is called the 'initial interaction'.

After the initial interaction, the remote process provides the prompt and answers commands.
This phase is called 'command interaction'.

During command interaction the output of the remote process can be broken up by screen-
length section and a 'more' promp is displayed. Local process has to answer this, usually
sending a single space character. See the UNIX/LINUX ``more`` command.

The remote process can interpret a 'exit' command, terminate then, and simulating EOF on
the pipe.
"""


import sys
import os
import time
import multiprocessing
import remote

import feeder_conf

#import the nbsr module (locally)
import nbsr


class RemoteError(Exception):
    pass
        
class Popen(object):
    """Simulate the subprocess.Popen class
    
    Instances of this class have stream attributes
    """
    def __init__(self, fd_to_read_from, fd_to_write_to):
        self.stdin = os.fdopen(fd_to_read_from, 'r', 100)
        self.stdout = os.fdopen(fd_to_write_to, 'w', 100)
        self.stderr = open(os.devnull, 'w')
    def terminate(self):
        self.stdin.close()
        self.stdout.close()
        self.stderr.close()


class Runner(object):
    def __init__(self):
        #lines or partial lines will be collected here from the pipe stream
        self.buffer = []
        
        conf = feeder_conf.conf
        
        #create 2 file descriptor pairs, and 2 bi-directional pipes
        pipe1_fds = os.pipe()
        pipe2_fds = os.pipe()
        self.local_pipe = Popen(pipe1_fds[0], pipe2_fds[1])
        self.remote_pipe = Popen(pipe2_fds[0], pipe1_fds[1])
        
        #create a remote process (not started yet)
        self.process = multiprocessing.Process(
                                                                target=remote.remoteprocess,
                                                                name='nbsr_feeder', 
                                                                kwargs=dict(pipe=self.remote_pipe, conf=conf), 
                                                            )
        
        #initial interaction communication definitions
        self.init_defs = (
            dict(
                case='prompt>', 
                response=None, 
            ), 
            dict(
                case='Password:', 
                response='pass123\n', 
            ), 
            dict(
                case=r'Store key in cache\? \(y/n\)', 
                response='y\n', 
            ), 
        )
        #extract the list of patterns (cases)
        self.init_patterns = tuple(p['case'] for p in self.init_defs)
        
        self.commandsgen = (c for c in
            (
                'command', 
                '9invalid command', 
                'givememore', 
                'errorcommand', 
                'exit'
            )
        )
        
        #command interaction communication definitions
        self.comms_defs = (
            dict(
                case='error', 
                response=RemoteError, 
            ), 
            dict(
                case='prompt>', 
                response=self._getnextcommand, 
            ), 
            dict(
                case='---More-', 
                response=' ', 
            ), 
        )
        #extract the list of patterns (cases)
        self.comms_patterns = tuple(p['case'] for p in self.comms_defs)

    
    def start(self):
        """Run test cases
        """
#        #open the pipe to feeder.py
#        self.pipe = subprocess.Popen(
#                                                ['python', 'test/feeder.py'], 
#                                                stdin=subprocess.PIPE, 
#                                                stdout=subprocess.PIPE, 
#                                                stderr=subprocess.STDOUT, 
#                                                shell=False, 
#                                            )
#        #If shell is True, a cmd shell is created where the remote process (feeder) runs.
#        #For some reason, when pipe is terminated, its output does not get closed and keeps
#        #  sending space characters. Therefore the nbsr working thread won't terminate
#        #  and this test run python script hangs even after sys.exit.
#        #So, better to use shell=False
        
        print self.remote_pipe.stdout
        #launch the remote process
        self.process.start()

        #create the nbsr stream and start reading the remote output
        with nbsr.NonBlockingStreamReader(self.local_pipe.stdin, timeout=3.5) as self.nb_stdin:
            #remember the start time as a base
            self.starttime = time.time()
            
            #wait until the remote process starts to send characters
            errortext = self._startup()
            
            #do initial interactions
            if not errortext:
                errortext = self._init_interaction()
            
            #do command interactions
            if not errortext:
                errortext = self._command_interaction()
        
        
        self.process.terminate()
        
        #terminate the pipes
        self.remote_pipe.terminate()
        self.local_pipe.terminate()
        
        #print the communication
        print ''.join(self.buffer)
        
        #exit the program
        sys.exit(errortext or 'OK')
    
    
    def _startup(self):
        """Start up the communication with the remote
        
        Returns:
            str: the error message, if any. Otherwise None
        """
        # wait up to 4.5 seconds for the remote to send data (feeder sets up in 3.7s)
        if not self.nb_stdin.is_sending(4.5):
            self._printstatus()
            return 'Remote not sending data'
        
        
    def _init_interaction(self):
        """Handle initial interaction
        
        Returns:
            str: the error message, if any. Otherwise None
        """
        while True:
            #read the remote output, store it and display progress
            self._read(self.init_patterns)
            self._printstatus()
            
            #if pattern matched, respond
            if self.nb_stdin.return_reason() == nbsr.returnby_PATTERN:
                idx = self.nb_stdin.found_pattern_info()[0]
                response = self.init_defs[idx]['response']
                #if there is no need to respond, exit initial interaction, otherwise respond
                if not response:
                    return
                else:
                    self._write(response)
                    continue
            
            #if there was a timeout (no any pattern matched, exit
            if self.nb_stdin.return_reason() == nbsr.returnby_TIMEOUT:
                return 'timeout'
            
            #if there was a timeout (no any pattern matched, exit
            if self.nb_stdin.return_reason() == nbsr.returnby_EOF:
                return 'EOF during initial interaction'
            
            else:
                return 'WTF?!'
        
        
    def _command_interaction(self):
        """Handle command interaction
        
        Returns:
            str: the error message, if any. Otherwise None
        """
        #get the first command
        sendthis = self._getnextcommand() + '\n'
        #loop on commands
        while True:
            #send the next command to the remote process
            self._write(sendthis)
            
            #read the remote output, store it and display progress
            self._read(self.comms_patterns)
            self._printstatus()
            
            if self.nb_stdin.return_reason() == nbsr.returnby_PATTERN:
                idx = self.nb_stdin.found_pattern_info()[0]
                response = self.comms_defs[idx]['response']
                #if response is a text, just send it
                if isinstance(response, (str,  unicode)):
                    sendthis = response
                    continue
                #if previous command was OK (e.g. prompt received), send next command
                elif response == self._getnextcommand:
                    sendthis = self._getnextcommand()
                    if not sendthis:
                        return
                    sendthis += '\n'
                    continue
                #if there was an error, raise the exception
                elif response == RemoteError:
                    raise RemoteError
            
            elif self.nb_stdin.return_reason() == nbsr.returnby_TIMEOUT:
                return 'timeout'
            
            elif self.nb_stdin.return_reason() == nbsr.returnby_EOF:
                return 'End of comms'
            
            else:
                return 'WTF?!'
    
    
    def _read(self, patternlist):
        self.buffer += list('<-<'+line for line in self.nb_stdin.readlines(expected_patterns=patternlist))
    
    
    def _write(self, output):
        self.local_pipe.stdout.write(output)
    
    
    def _getnextcommand(self):
        try:
            return self.commandsgen.next()
        except StopIteration:
            return None
    
    
    def _printstatus(self):
        """Print a formatted status after a read"""
        elapsed = time.time()-self.starttime
        
        reason = self.nb_stdin.return_reason()
        
        if reason == nbsr.returnby_PATTERN:
            idx = self.nb_stdin.found_pattern_info()[0]
            details = 'returned by matching pattern index ' + str(idx)
        
        elif reason == nbsr.returnby_SIZE:
            details = 'returned because enough data read, which is normal'
        
        elif reason == nbsr.returnby_NEWLINE:
            details = 'returned because of new line, which is normal'
            
        elif reason == nbsr.returnby_TIMEOUT:
            details = 'returned because of timeout'
        
        elif reason == nbsr.returnby_EOF:
            details = 'returned because of EOF'
        
        else:
            details = 'what reason:' + str(reason)
        
        #print the elapsed time and the reason of return of the reading function
        self.buffer.append('>->%.9f: %s'% (elapsed, details))
